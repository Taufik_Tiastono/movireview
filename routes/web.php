<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

// Route::get('/', function(){
//     return view('layout.master');
// });

Route::get('/', 'FilmController@index');

// Route::get('/film', function(){
//     return view('film.index');
// });

Route::group(['middleware' => ['auth']], function () {
    Route::resource('genre','GenreController');

    //Update Profile
    Route::resource('profile','ProfileController')->only([
        'index','update']);

    Route::resource('kritik','KritikController')->only([
        'store']);    
             
});    

Route::resource('film','FilmController');


Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
