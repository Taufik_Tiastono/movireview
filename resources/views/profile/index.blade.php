@extends('layout.master') 
@section('carousel')

<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="/film"><i class="fa fa-home"></i> Home</a>
                    <a href="/profile">Profile</a>
                    <span>Create Film</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('judul')
<div class="section-title">
    <h4>Update Profile</h4>
</div>
@endsection
 @push('script')
<script src="https://cdn.tiny.cloud/1/dqywznnnbbirwxo9qbj9gm9ic6ejgdveunl4s53fwyu4n56j/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
      selector: 'textarea',
      //plugins: 'a11ychecker advcode casechange export formatpainter linkchecker autolink lists checklist media mediaembed pageembed permanentpen powerpaste table advtable tinycomments tinymcespellchecker',
      //toolbar: 'a11ycheck addcomment showcomments casechange checklist code export formatpainter pageembed permanentpen table',
      plugins : 'advlist autolink lists link image charmap print priview hr anchor pageblank',
      toolbar_mode: 'floating',
      //tinycomments_mode: 'embedded',
      //tinycomments_author: 'Author name',  
    });

</script>



@endpush 
@section('content')
<form action="/profile/{{$profile->id}}" method="POST" style="color:whitesmoke">
    @csrf @method('PUT')

    <div class="form-group">
        <label>Nama</label>
        <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="email" class="form-control" value="{{$profile->user->email}}" disabled>
    </div>

    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" class="form-control" value="{{$profile->umur}}" id="#">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" id="#">{{$profile->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" class="form-control" id="#">{{$profile->alamat}}</textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-warning">Update</button>
</form>
@endsection
 
@section('sidebar')
<div class="col-lg-4 col-md-6 col-sm-8">
    <div class="product__sidebar">
        <div class="product__sidebar__view">
            <div class="section-title">
                <h5>Review Film</h5>
            </div>
            <ul class="filter__controls">
                <li class="active" data-filter="*">Day</li>
                <li data-filter=".week">Week</li>
                <li data-filter=".month">Month</li>
                <li data-filter=".years">Years</li>
            </ul>
            <div class="filter__gallery">
                <div class="product__sidebar__view__item set-bg mix years month" data-setbg="{{asset('anime-main/img/sidebar/tv-4.jpg')}}">
                    <div class="ep">18 / ?</div>
                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    <h5><a href="#">Fate/stay night: Heaven's Feel I. presage flower</a></h5>
                </div>
                <div class="product__sidebar__view__item set-bg mix day" data-setbg="{{asset('anime-main/img/sidebar/tv-5.jpg')}}">
                    <div class="ep">18 / ?</div>
                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    <h5><a href="#">Fate stay night unlimited blade works</a></h5>
                </div>
            </div>
        </div>
        <div class="product__sidebar__comment">
            <div class="section-title">
                <h5>New Comment</h5>
            </div>
            <div class="product__sidebar__comment__item">
                <div class="product__sidebar__comment__item__pic">
                    <img src="{{asset('anime/img/sidebar/comment-3.jpg')}}" alt="">
                </div>
                <div class="product__sidebar__comment__item__text">
                    <ul>
                        <li>Active</li>
                        <li>Movie</li>
                    </ul>
                    <h5><a href="#">Kizumonogatari III: Reiket su-hen</a></h5>
                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                </div>
            </div>
            <div class="product__sidebar__comment__item">
                <div class="product__sidebar__comment__item__pic">
                    <img src="{{asset('anime-main/img/sidebar/comment-4.jpg')}}" alt="">
                </div>
                <div class="product__sidebar__comment__item__text">
                    <ul>
                        <li>Active</li>
                        <li>Movie</li>
                    </ul>
                    <h5><a href="#">Monogatari Series: Second Season</a></h5>
                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection