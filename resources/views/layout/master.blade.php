<!DOCTYPE html>
<html lang="zxx">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="Anime Template">
    <meta name="keywords" content="Anime, unica, creative, html">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>movireview</title>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Mulish:wght@300;400;500;600;700;800;900&display=swap" rel="stylesheet">

    <!-- Css Styles -->
    <link rel="stylesheet" href="{{asset('anime-main/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/font-awesome.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/elegant-icons.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/plyr.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/nice-select.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/owl.carousel.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/slicknav.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('anime-main/css/style.css')}}" type="text/css"> @stack('style')
</head>

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>

    <!-- Header Section Begin -->
    @include('partial.header')
    <!-- Header End -->

    <!-- Hero Section Begin -->
    {{--
    @include('partial.carousel') --}}
    <section>
        @yield('carousel')
    </section>
    <!-- Hero Section End -->

    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <div class="trending__product">

                        <div class="row">
                            <div class="col-lg-8 col-md-8 col-sm-8">


                                @yield('judul')

                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-4">
                                <div class="btn__all">
                                    @yield('tambah')
                                </div>
                            </div>
                        </div>
                        <span>
                        @yield('content')
                        </span>
                    </div>
                </div>
                @yield('sidebar') {{--
                <div class="col-lg-4 col-md-6 col-sm-8">
                    <div class="product__sidebar">
                        <div class="product__sidebar__view">
                            <div class="section-title">
                                <h5>Top Views</h5>
                            </div>
                            <ul class="filter__controls">
                                <li class="active" data-filter="*">Day</li>
                                <li data-filter=".week">Week</li>
                                <li data-filter=".month">Month</li>
                                <li data-filter=".years">Years</li>
                            </ul>
                            <div class="filter__gallery">
                                <div class="product__sidebar__view__item set-bg mix years month" data-setbg="img/sidebar/tv-4.jpg">
                                    <div class="ep">18 / ?</div>
                                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                                    <h5><a href="#">Fate/stay night: Heaven's Feel I. presage flower</a></h5>
                                </div>
                                <div class="product__sidebar__view__item set-bg mix day" data-setbg="img/sidebar/tv-5.jpg">
                                    <div class="ep">18 / ?</div>
                                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                                    <h5><a href="#">Fate stay night unlimited blade works</a></h5>
                                </div>
                            </div>
                        </div>
                        <div class="product__sidebar__comment">
                            <div class="section-title">
                                <h5>New Comment</h5>
                            </div>
                            <div class="product__sidebar__comment__item">
                                <div class="product__sidebar__comment__item__pic">
                                    <img src="img/sidebar/comment-3.jpg" alt="">
                                </div>
                                <div class="product__sidebar__comment__item__text">
                                    <ul>
                                        <li>Active</li>
                                        <li>Movie</li>
                                    </ul>
                                    <h5><a href="#">Kizumonogatari III: Reiket su-hen</a></h5>
                                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                                </div>
                            </div>
                            <div class="product__sidebar__comment__item">
                                <div class="product__sidebar__comment__item__pic">
                                    <img src="img/sidebar/comment-4.jpg" alt="">
                                </div>
                                <div class="product__sidebar__comment__item__text">
                                    <ul>
                                        <li>Active</li>
                                        <li>Movie</li>
                                    </ul>
                                    <h5><a href="#">Monogatari Series: Second Season</a></h5>
                                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
    <!-- Product Section End -->

    <!-- Footer Section Begin -->
    @include('partial.footer')
    <!-- Footer Section End -->

    <!-- Search model Begin -->
    <div class="search-model">
        <div class="h-100 d-flex align-items-center justify-content-center">
            <div class="search-close-switch"><i class="icon_close"></i></div>
            <form class="search-model-form">
                <input type="text" id="search-input" placeholder="Search here.....">
            </form>
        </div>
    </div>
    <!-- Search model end -->

    <!-- Js Plugins -->
    <script src="{{asset('anime-main/js/jquery-3.3.1.min.js')}}"></script>
    <script src="{{asset('anime-main/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('anime-main/js/player.js')}}"></script>
    <script src="{{asset('anime-main/js/jquery.nice-select.min.js')}}"></script>
    <script src="{{asset('anime-main/js/mixitup.min.js')}}"></script>
    <script src="{{asset('anime-main/js/jquery.slicknav.js')}}"></script>
    <script src="{{asset('anime-main/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('anime-main/js/main.js')}}"></script>

    @stack('script')


</body>

</html>