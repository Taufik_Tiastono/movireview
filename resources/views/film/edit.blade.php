@extends('layout.master') 
@section('carousel')
<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="/film"><i class="fa fa-home"></i> Home</a>
                    <a href="/film">Film</a>
                    <span>Create Film</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('judul')
<div class="section-title">
    <h4>Update Film</h4>
</div>
@endsection
 @push('style')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> 
@endpush @push('script')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    // In your Javascript (external .js resource or <script> tag)
$(document).ready(function() {
    $('.js-example-basic-single').select2();
});

</script>


@endpush 
@section('content')

<form action="/film/{{$film->id}}" method="POST" enctype="multipart/form-data" style="color:whitesmoke">
    @csrf @method('PUT')
    <div class="form-group">
        <label>Judul Film</label>
        <input type="text" name="judul" class="form-control" value="{{$film->judul}}" id="#">
    </div>
    @error('judul')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Ringkasan</label>
        <textarea name="ringkasan" class="form-control" cols="30" rows="10">{{$film->ringkasan}}</textarea>
    </div>
    @error('ringkasan')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Tahun</label>
        <input type="text" name="tahun" class="form-control" value="{{$film->tahun}}" id="#">
    </div>
    @error('tahun')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Poster</label>
        <input type="file" name="poster" class="form-control" id="#">
    </div>
    @error('poster')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror


    <div class="form-group">
        <label>Genre</label><br>
        <select name="genre_id" id="" class="js-example-basic-single" style="width:100%">
            <option value="">--Pilih Kategori--</option>
            @foreach ($genre as $item)
                @if($item->id == $film->genre_id)
                    <option value="{{$item->id}}" selected>{{$item->nama}}</option>
                @else
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @endif
            @endforeach
        </select>
    </div>
    @error('genre_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <br>
    <br>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection