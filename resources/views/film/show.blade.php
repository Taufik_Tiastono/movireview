@extends('layout.master') 
@section('carousel')
<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="/film"><i class="fa fa-home"></i> Home</a>
                    <a href="/film">Film</a>
                    <span>Detail Film</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('judul')
<div class="section-title">
    <h4>Detail Film</h4>
</div>
@endsection
 
@section('content')
<section class="anime-details spad">
    <div class="container">
        <div class="anime__details__content">
            <div class="row">
                <div class="col-lg-5">
                    <div class="anime__details__pic set-bg" data-setbg="{{asset('poster/'.$film->poster)}}">
                        <div class="comment"><i class="fa fa-comments"></i> 11</div>
                        <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="anime__details__text">
                        <div class="anime__details__title">
                            <h3>{{$film->judul}}</h3>
                            <span></span>
                        </div>

                        <p>{{$film->ringkasan}}</p>
                        <div class="anime__details__widget">
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                    <ul>
                                        <li><span>Type:</span> Movie</li>
                                        <li><span>Studios:</span> Lerche</li>
                                        <li><span>Date aired:</span> {{$film->tahun}}</li>
                                        <li><span>Status:</span> Airing</li>
                                        <li><span>Genre:</span> {{$film->genre->nama}}</li>
                                    </ul>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <ul>
                                        <li><span>Scores:</span> 7.31 / 1,515</li>
                                        <li><span>Rating:</span> 8.5 / 161 times</li>
                                        <li><span>Duration:</span> 24 min/ep</li>
                                        <li><span>Quality:</span> HD</li>
                                        <li><span>Views:</span> 131,541</li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <div class="anime__details__btn">
                            {{-- <a href="#" class="follow-btn"><i class="fa fa-heart-o"></i> Follow</a>
                            <a href="#" class="watch-btn"><span>Watch Now</span> <i
                                    class="fa fa-angle-right"></i></a> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8">
                <div class="anime__details__review">
                    <div class="section-title">
                        <h5>Reviews</h5>
                    </div>
                    @forelse($film->kritik as $item)
                    <div class="anime__review__item">
                        <div class="anime__review__item__pic">
                            <img src="img/anime/review-1.jpg" alt="">
                        </div>
                        <div class="anime__review__item__text">
                            <h6>{{$item->user->name}} - <span><i class="fa fa-star" style="color:gold"></i> {{$item->point}} /10</span></h6>
                            <p>{{$item->isi}}</p>
                        </div>
                    </div>
                    @empty
                    <h4 style="color:whitesmoke">Tidak ada Riview </h4>
                    @endforelse

                </div>
                <form action="/kritik" method="POST" style="color:whitesmoke">
                    @csrf
                    <div class="form-group">
                        <label>Tinggalkan Komentarmu</label> {{-- <input type="hidden" value="{{$user->id}}" name="user_id">                        --}}
                        <input type="hidden" value="{{$film->id}}" name="film_id">

                        <textarea name="isi" class="form-control"></textarea>
                    </div>
                    @error('isi')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group">
                        <label>Berikan Penilaian 1 s.d 10</label>
                        <input type="number" name="point" class="form-control" id="#">
                    </div>
                    @error('point')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>

        </div>
    </div>
</section>
@endsection
 
@section('sidebar')
<div class="col-lg-4 col-md-6 col-sm-8">
    <div class="product__sidebar">
        <div class="product__sidebar__view">
            <div class="section-title">
                <h5>Iklan Poster</h5>
            </div>
            <ul class="filter__controls">
                <li class="active" data-filter="*">Day</li>
                <li data-filter=".week">Week</li>
                <li data-filter=".month">Month</li>
                <li data-filter=".years">Years</li>
            </ul>
            <div class="filter__gallery">
                <div class="product__sidebar__view__item set-bg mix years month" data-setbg="{{asset('anime-main/img/sidebar/tv-4.jpg')}}">
                    <div class="ep">18 / ?</div>
                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    <h5><a href="#">Fate/stay night: Heaven's Feel I. presage flower</a></h5>
                </div>
                <div class="product__sidebar__view__item set-bg mix day" data-setbg="{{asset('anime-main/img/sidebar/tv-5.jpg')}}">
                    <div class="ep">18 / ?</div>
                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    <h5><a href="#">Fate stay night unlimited blade works</a></h5>
                </div>
            </div>
        </div>
        <div class="product__sidebar__comment">
            <div class="section-title">
                <h5>Advertisement</h5>
            </div>
            <div class="product__sidebar__comment__item">
                <div class="product__sidebar__comment__item__pic">
                    <img src="{{asset('anime-main/img/sidebar/comment-3.jpg')}}" alt="">
                </div>
                <div class="product__sidebar__comment__item__text">
                    <ul>
                        <li>Active</li>
                        <li>Movie</li>
                    </ul>
                    <h5><a href="#">Kizumonogatari III: Reiket su-hen</a></h5>
                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                </div>
            </div>
            <div class="product__sidebar__comment__item">
                <div class="product__sidebar__comment__item__pic">
                    <img src="{{asset('anime-main/img/sidebar/comment-4.jpg')}}" alt="">
                </div>
                <div class="product__sidebar__comment__item__text">
                    <ul>
                        <li>Active</li>
                        <li>Movie</li>
                    </ul>
                    <h5><a href="#">Monogatari Series: Second Season</a></h5>
                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection