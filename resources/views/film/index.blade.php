@extends('layout.master') 
@section('carousel')
<section class="hero">
    <div class="container">
        <div class="hero__slider owl-carousel">
            <div class="hero__items set-bg" data-setbg="{{asset('anime-main/img/hero/hero-1.jpg')}}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <div class="label">movireview</div>
                            <h2>Berikan ULASAN terbaik mu...</h2>
                            <p>dan berikan rating ...</p>
                            <a href="#"><span>Review Now</span> <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero__items set-bg" data-setbg="{{asset('anime-main/img/hero/hero-1.jpg')}}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <div class="label">movireview</div>
                            <h2>Movireview Movireview Movireview </h2>
                            <p>review review review..</p>
                            <a href="#"><span>Rating Now</span> <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero__items set-bg" data-setbg="{{asset('anime-main/img/hero/hero-1.jpg')}}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <div class="label">movireview</div>
                            <h2>Terimakasih dari kami ...</h2>
                            <p>movireview</p>
                            <a href="#"><span>Review Now</span> <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
 
@section('judul')
<div class="section-title">
    <h4>Daftar Film</h4>
</div>
@endsection
 
@section('tambah') @auth
<a href="/film/create" class="btn btn-success btn-md mb-3">Tambah Data</a>@endauth
@endsection
 @push('style')
<style>
    .btn-group-xs>.btn,
    .btn-xs {
        padding: 1px 5px;
        font-size: 12px;
        line-height: 1.5;
        border-radius: 3px;
    }
</style>




@endpush 
@section('content')
<div class="row">
    @forelse($film as $item)
    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="product__item">
            <div class="product__item__pic set-bg" data-setbg="{{asset('poster/'.$item->poster)}}">
                <div class="ep"><i class="fa fa-star" style="color:goldenrod"></i> 8/10</div>
                <div class="comment"><i class="fa fa-comments"></i> 11</div>
                <div class="view"><i class="fa fa-eye"></i> 9141</div>
            </div>
            <div class="product__item__text">
                <ul>
                    <li>Movie</li>
                    <li>{{$item->genre->nama}}</li>
                </ul>
                <h5><a href="#">{{$item->judul}} ({{$item->tahun}})</a></h5>
                @auth
                <form action="/film/{{$item->id}}" method="POST">
                    @csrf @method('delete')
                    <a href="/film/{{$item->id}}" class="btn btn-info btn-xs">Detail</a>
                    <a href="/film/{{$item->id}}/edit" class="btn btn-warning btn-xs">Edit</a>
                    <input type="submit" class="btn btn-danger btn-xs" value="Delete"></a>
                </form>
                @endauth @guest <a href="/film/{{$item->id}}" class="btn btn-info btn-xs">Detail</a> @endguest
            </div>

        </div>
    </div>
    @empty
    <h2 style="color:whitesmoke">Data Film Masih Kosong ...</h2>
    @endforelse

</div>
@endsection
 
@section('sidebar')
<div class="col-lg-4 col-md-6 col-sm-8">
    <div class="product__sidebar">
        <div class="product__sidebar__view">
            <div class="section-title">
                <h5>Iklan Poster</h5>
            </div>
            <ul class="filter__controls">
                <li class="active" data-filter="*">Day</li>
                <li data-filter=".week">Week</li>
                <li data-filter=".month">Month</li>
                <li data-filter=".years">Years</li>
            </ul>
            <div class="filter__gallery">
                <div class="product__sidebar__view__item set-bg mix years month" data-setbg="{{asset('anime-main/img/sidebar/tv-4.jpg')}}">
                    <div class="ep">18 / ?</div>
                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    <h5><a href="#">Fate/stay night: Heaven's Feel I. presage flower</a></h5>
                </div>
                <div class="product__sidebar__view__item set-bg mix day" data-setbg="{{asset('anime-main/img/sidebar/tv-5.jpg')}}">
                    <div class="ep">18 / ?</div>
                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    <h5><a href="#">Fate stay night unlimited blade works</a></h5>
                </div>
            </div>
        </div>
        <div class="product__sidebar__comment">
            <div class="section-title">
                <h5>Advertisement</h5>
            </div>
            <div class="product__sidebar__comment__item">
                <div class="product__sidebar__comment__item__pic">
                    <img src="{{asset('anime-main/img/sidebar/comment-3.jpg')}}" alt="">
                </div>
                <div class="product__sidebar__comment__item__text">
                    <ul>
                        <li>Active</li>
                        <li>Movie</li>
                    </ul>
                    <h5><a href="#">Kizumonogatari III: Reiket su-hen</a></h5>
                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                </div>
            </div>
            <div class="product__sidebar__comment__item">
                <div class="product__sidebar__comment__item__pic">
                    <img src="{{asset('anime-main/img/sidebar/comment-4.jpg')}}" alt="">
                </div>
                <div class="product__sidebar__comment__item__text">
                    <ul>
                        <li>Active</li>
                        <li>Movie</li>
                    </ul>
                    <h5><a href="#">Monogatari Series: Second Season</a></h5>
                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection