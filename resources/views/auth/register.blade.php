@extends('layout.master') 
@section('carousel')

<!-- Header End -->

<!-- Normal Breadcrumb Begin -->
<section class="normal-breadcrumb set-bg" data-setbg="{{asset('anime-main/img/normal-breadcrumb.jpg')}}">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <div class="normal__breadcrumb__text">
                    <h2>Sign Up</h2>
                    <p>Welcome to Movireview.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Normal Breadcrumb End -->

<!-- Signup Section Begin -->
<section class="signup spad">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="login__form">

                    <h3>&nbsp;&nbsp;&nbsp;&nbsp; Sign Up Movireview</h3>

                    <form action="{{ route('register') }}" method="post">
                        @csrf
                        <div class="input__item">
                            <input type="email" placeholder="Email address" name="email">
                            <span class="icon_mail"></span>
                        </div>
                        @error('email')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                        <div class="input__item">
                            <input type="text" placeholder="Your Name" name="name">
                            <span class="icon_profile"></span>
                        </div>
                        @error('name')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                        <div class="input__item">
                            <input type="password" placeholder="Password" name="password">
                            <span class="icon_lock"></span>
                        </div>
                        @error('password')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                        <div class="input__item">
                            <input type="password" placeholder="Retype Password" name="password_confirmation">
                            <span class="icon_lock"></span>
                        </div>
                        <div class="input__item">
                            <input type="number" placeholder="your age" name="umur">
                            <span class="icon_profile"></span>
                        </div>
                        @error('umur')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                        <div class="input__item">
                            <textarea name="bio" class="form-control" placeholder="Biografi"></textarea>
                        </div>
                        @error('bio')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror
                        <div class="input__item">
                            <textarea name="alamat" class="form-control" placeholder="Address"></textarea>
                        </div>
                        @error('alamat')
                        <div class="alert alert-danger">{{$message}}</div>
                        @enderror


                        <button type="submit" class="site-btn">Sign Up Now</button>
                    </form>

                    <h5>Already have an account? <a href="/login">Log In!</a></h5>
                </div>
            </div>
            {{--
            <div class="col-lg-6">
                <div class="login__social__links">
                    <h3>Login With:</h3>
                    <ul>
                        <li><a href="#" class="facebook"><i class="fa fa-facebook"></i> Sign in With Facebook</a>
                        </li>
                        <li><a href="#" class="google"><i class="fa fa-google"></i> Sign in With Google</a></li>
                        <li><a href="#" class="twitter"><i class="fa fa-twitter"></i> Sign in With Twitter</a></li>
                    </ul>
                </div>
            </div> --}} {{--
            <div class="col-lg-6">
                <div class="login__register">
                    <h3>I Have An Account?</h3>
                    <a href="/login" class="primary-btn">Login Now</a>
                </div>
            </div> --}}
        </div>
    </div>
</section>
@endsection