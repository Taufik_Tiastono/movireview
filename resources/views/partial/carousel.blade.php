<section class="hero">
    <div class="container">
        <div class="hero__slider owl-carousel">
            <div class="hero__items set-bg" data-setbg="{{asset('anime-main/img/hero/hero-1.jpg')}}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <div class="label">movireview</div>
                            <h2>Berikan ULASAN terbaik mu...</h2>
                            <p>dan berikan rating ...</p>
                            <a href="#"><span>Review Now</span> <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero__items set-bg" data-setbg="{{asset('anime-main/img/hero/hero-1.jpg')}}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <div class="label">movireview</div>
                            <h2>Movireview Movireview Movireview </h2>
                            <p>review review review..</p>
                            <a href="#"><span>Rating Now</span> <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero__items set-bg" data-setbg="{{asset('anime-main/img/hero/hero-1.jpg')}}">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="hero__text">
                            <div class="label">movireview</div>
                            <h2>Terimakasih dari kami ...</h2>
                            <p>movireview</p>
                            <a href="#"><span>Review Now</span> <i class="fa fa-angle-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>