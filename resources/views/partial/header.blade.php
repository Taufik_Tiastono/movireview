<header class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-2">
                <div class="header__logo">
                    <a href="/">
                        <img src="{{asset('anime-main/img/movireview.png')}}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-lg-7">
                <div class="header__nav">
                    <nav class="header__menu mobile-menu">
                        <ul>
                            <li><a href="/film">Film</a></li>
                            @auth
                            <li><a href="/genre">Categories </a> {{--
                                <ul class="dropdown">
                                    <li><a href="./categories.html">Categories<span class="arrow_carrot-down"></span>   </a></li>
                                    <li><a href="./anime-details.html">Anime Details</a></li>
                                    <li><a href="./anime-watching.html">Anime Watching</a></li>
                                    <li><a href="./blog-details.html">Blog Details</a></li>
                                    <li><a href="./signup.html">Sign Up</a></li>
                                    <li><a href="./login.html">Login</a></li>
                                </ul> --}}
                            </li>
                            @endauth
                        </ul>
                    </nav>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="header__nav">
                    <nav class="header__menu mobile-menu">
                        @guest
                        <ul>
                            {{-- <a href="#" class="search-switch"><span class="icon_search"></span></a> --}}
                            <li> <a href=""><span class="icon_profile"></span> Belum login</a>
                                <ul class="dropdown">
                                    <li><a href="/login"><i class="fa fa-sign-in"></i> Sign in</a></li>
                                </ul>
                            </li>
                        </ul>@endguest @auth
                        <ul>
                            <li><a href="#"><span class="icon_profile"></span>  {{ Auth::user()->name }}</a>
                                <ul class="dropdown">
                                    <li><a href="/profile">Akun Profile</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();">
                                            <p style="color:dark"><i class="fa fa-sign-out" style="color:black"></i> Sign out</p>
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        @endauth
                    </nav>
                </div>
            </div>
        </div>
        <div id="mobile-menu-wrap"></div>
    </div>
</header>