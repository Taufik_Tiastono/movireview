@extends('layout.master') 
@section('carousel')
<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="/film"><i class="fa fa-home"></i> Home</a>
                    <a href="/genre">Categories</a>
                    <span>Update Genre</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('judul')
<div class="section-title">
    <h4>Update Genre</h4>
</div>
@endsection
 
@section('content')
<form action="/genre/{{$genre->id}}" method="POST">
    @csrf @method('PUT')
    <div class="form-group" style="color:white">
        <label>Nama Genre</label>
        <input type="text" name="nama" value="{{$genre->nama}}" class="form-control" id="#">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-warning">Update</button>
</form>
@endsection