@extends('layout.master') 
@section('carousel')
<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="/"><i class="fa fa-home"></i> Home</a>
                    <a href="/genre">Categories</a>
                    <span>Detail Genre</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('judul')

<div class="section-title">
    <h4>Daftar FIlm Genre {{$genre->nama}}</h4>
</div>
@endsection
 
@section('content')

<div class="row">
    {{-- karena listnya lebih dari satu menggunakan foreach --}} @foreach($genre->film as $item)

    <div class="col-lg-4 col-md-6 col-sm-6">
        <div class="product__item">
            <div class="product__item__pic set-bg" data-setbg="{{asset('poster/'.$item->poster)}}">
                <div class="ep"><i class="fa fa-star" style="color:goldenrod"></i> 8/10</div>
                <div class="comment"><i class="fa fa-comments"></i> 11</div>
                <div class="view"><i class="fa fa-eye"></i> 9141</div>
            </div>
            <div class="product__item__text">
                <ul>
                    <li>Movie</li>
                    <li>{{$item->genre->nama}}</li>
                </ul>
                <h5><a href="#">{{$item->judul}} ({{$item->tahun}})</a></h5>
                <p class="card-text" style="color:whitesmoke">{{ Str::limit($item->ringkasan, 100,'') }} <a href="/film/{{$item->id}}">Read more...</a></p>
            </div>

        </div>
    </div>
    @endforeach
</div>
@endsection
 
@section('sidebar')
<div class="col-lg-4 col-md-6 col-sm-8">
    <div class="product__sidebar">
        <div class="product__sidebar__view">
            <div class="section-title">
                <h5>Iklan Poster</h5>
            </div>
            <ul class="filter__controls">
                <li class="active" data-filter="*">Day</li>
                <li data-filter=".week">Week</li>
                <li data-filter=".month">Month</li>
                <li data-filter=".years">Years</li>
            </ul>
            <div class="filter__gallery">
                <div class="product__sidebar__view__item set-bg mix years month" data-setbg="{{asset('anime-main/img/sidebar/tv-4.jpg')}}">
                    <div class="ep">18 / ?</div>
                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    <h5><a href="#">Fate/stay night: Heaven's Feel I. presage flower</a></h5>
                </div>
                <div class="product__sidebar__view__item set-bg mix day" data-setbg="{{asset('anime-main/img/sidebar/tv-5.jpg')}}">
                    <div class="ep">18 / ?</div>
                    <div class="view"><i class="fa fa-eye"></i> 9141</div>
                    <h5><a href="#">Fate stay night unlimited blade works</a></h5>
                </div>
            </div>
        </div>
        <div class="product__sidebar__comment">
            <div class="section-title">
                <h5>Advertisement</h5>
            </div>
            <div class="product__sidebar__comment__item">
                <div class="product__sidebar__comment__item__pic">
                    <img src="{{asset('anime-main/img/sidebar/comment-3.jpg')}}" alt="">
                </div>
                <div class="product__sidebar__comment__item__text">
                    <ul>
                        <li>Active</li>
                        <li>Movie</li>
                    </ul>
                    <h5><a href="#">Kizumonogatari III: Reiket su-hen</a></h5>
                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                </div>
            </div>
            <div class="product__sidebar__comment__item">
                <div class="product__sidebar__comment__item__pic">
                    <img src="{{asset('anime-main/img/sidebar/comment-4.jpg')}}" alt="">
                </div>
                <div class="product__sidebar__comment__item__text">
                    <ul>
                        <li>Active</li>
                        <li>Movie</li>
                    </ul>
                    <h5><a href="#">Monogatari Series: Second Season</a></h5>
                    <span><i class="fa fa-eye"></i> 19.141 Viewes</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection