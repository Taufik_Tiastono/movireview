@extends('layout.master') 
@section('carousel')
<div class="breadcrumb-option">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb__links">
                    <a href="/"><i class="fa fa-home"></i> Home</a>
                    <a href="/genre">Categories</a>
                    <span>Daftar Genre</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
 
@section('judul')

<div class="section-title">
    <h4>Daftar Genre</h4>
</div>
@endsection
 
@section('tambah')
<a href="/genre/create" class="btn btn-success btn-md mb-3">Tambah Data</a>
@endsection
 @push('script')
<script src="{{asset('anime-main/plugins/datatables/jquery.dataTables.js')}}"></script>

<script src="{{asset('anime-main/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>

<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.js"></script>

<script>
    $(function () {
    $("#example1").DataTable();
  });

</script>



@endpush @push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css" /> 
@endpush 
@section('content')


<table id="example1" class="table table-hover table-striped" style="color:whitesmoke">
    {{--
    <div class="container-fluid"> --}}
        <thead class="thead-dark">
            <tr class="row">
                <th class="col">#</th>
                <th class="col">Nama Genre</th>
                <th class="col text-center">Action</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($genre as $key => $item)
            <tr class="row" style="color:white">
                <td class="col">{{$key +1}}</td>
                <td class="col">{{$item->nama}}</td>
                <td class="col text-center">
                    <form action="/genre/{{$item->id}}" method="POST">
                        <a href="/genre/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/genre/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a> @method('delete') @csrf
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
            @empty
            <tr class="row">
                <td class="col">Data Masih Kosong</td>
            </tr>
            @endforelse
        </tbody>
    </div>
</table>
@endsection