<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Genre;
use RealRashid\SweetAlert\Facades\Alert;

class GenreController extends Controller
{
    //
    public function create(){
        return view('genre.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required'
        ]);

        $genre = new Genre;
        $genre->nama = $request->nama;

        //Alert::success('Berhasil', 'Tambah Data Genre Berhasil');
        $genre->save();
        return redirect('/genre');
    }

    public function index(){
        //query builder
        // $genre = DB::table('genre')->get();
        
        //ORM
        $genre = Genre::all();
        return view('genre.index', compact('genre'));
       
    }

    public function show($id){
        // query builder
        // $genre = DB::table('genre')->where('id' , $id)->first();
        
        //ORM
        $genre = Genre::find($id);
        return view('genre.show', compact('genre'));
    }

    public function edit($id){
        $genre = Genre::findOrFail($id);
        return view('genre.edit', compact('genre'));
    }

    public function update($id, Request $request){
        $request->validate([
            'nama' => 'required'
        ]);

        $genre = Genre::find($id);
        $genre->nama = $request->nama;
        //Alert::success('Berhasil', 'Update Data Genre Berhasil');
        $genre->save();        
        return redirect('/genre');        

    }

    public function destroy($id)
    {
        //
        $genre = Genre::find($id);

        $genre->delete();

        return redirect('/genre');
    }
}
